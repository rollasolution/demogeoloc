angular.module('starter.controllers', ['ionic'])

.controller('DashCtrl', function($scope, $ionicPopup) {

    
$scope.data = {};
$scope.showAlert = function() {
  			   var alertPopup = $ionicPopup.alert({
  			     title: 'Render Information',
  			     template: 'Please, test me in multiple screen sizes'
  			   });
		};    
})

.controller('LocCtrl', ['$scope','GoogleMapApi'.ns(), '$cordovaGeolocation', '$ionicLoading', '$state', 'Loc', function($scope, GoogleMapApi, $cordovaGeolocation, $ionicLoading, $state, Loc) {
    
        $scope.loadingIndicator = $ionicLoading.show({
            content: 'Loading Data',
            animation: 'fade-in',
            showBackdrop: false,
            maxWidth: 200,
            showDelay: 500
        });
    
    
        $cordovaGeolocation.getCurrentPosition().then(function (position) {
            
                var lat  = position.coords.latitude;
                var lng = position.coords.longitude;
         
                $scope.map = {center: {latitude: lat, longitude: lng }, zoom: 15, bounds: {}};
                $scope.options = {scrollwheel: true};

                $scope.myPosition = {
                    id: 0,
                    coords: {
                    latitude: lat,
                    longitude: lng
                    },
                    icon: 'img/icons/Location-Map-icon-icon.png',
                    options: { draggable: false },
                    optimized: false,
                    events: {
                    }
                }
            
                $scope.markers = Loc.all();
            
                /*
                * GoogleMapApi is a promise with a
                * then callback of the google.maps object
                *   @pram: maps = google.maps
                */
                GoogleMapApi.then(function(maps) {
                    $ionicLoading.hide();
                });
                
              
            }, function(err) {
              // error
            });
            
        
        $scope.relocate = function() {
            $state.go($state.current, {}, {reload: true});
         
		};

        
    
}])

.controller('PosCtrl', function($scope, Pos) {
 
  $scope.pos = Pos.all();    
})

.controller('PosDetailCtrl', function($scope, $stateParams, Pos) {
    
  $scope.pos = Pos.get($stateParams.id);
})

.controller('NewsCtrl', function($scope, News) {

    $scope.items = News.all();
})

.controller('ConfCtrl', function($scope) {
    
});


