angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Pos', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var pos = [
    { id: 0, name: 'Don Julio', address: 'Guatemala 2240', lat: '', lng:'' },
    { id: 1, name: 'Parollaccia', address: '', lat: '', lng:'' },
    { id: 2, name: 'Mc Donnals', address: '', lat: '', lng:'' },
    { id: 3, name: 'Rollasolution', address: '', lat: '', lng:'' }
  ];

  return {
    all: function() {
      return pos;
    },
    get: function(id) {
      // Simple index lookup
      return pos[id];
    }
  }
})

/**
 * A simple example service that returns some data.
 */
.factory('Loc', function() {
  // Might use a resource here that returns a JSON array
    
  // Some fake testing data
  var markers = [];

  return {
    all: function() {
    
    var marker1 = {
        id:0,  
        coords: {
            latitude: -34.59365,
            longitude: -58.451089
          },
         icon:"img/icons/marker.png"  
    };
    
    markers.push(marker1);
    
    var marker2 = {
        id:1,  
        coords: {
            latitude: -34.59713,
            longitude: -58.448664
          },
         icon:"img/icons/marker.png"  
    };
        
    markers.push(marker2);
        
    var marker3 = {
        id:2,  
        coords: {
            latitude: -34.5937362,
            longitude: -58.4445694
          },
         icon:"img/icons/marker.png"  
    };
        
   
    markers.push(marker3);
    
    return markers;
    
    },
    get: function(id) {
      // Simple index lookup
      return markers[id];
    }
  }
})

/**
 * A simple example service that returns some data.
 */
.factory('News', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var items = [
    { id: 0, name: 'Don Julio', description:'Valid benefit by submitting Club La Nación PREMIUM or Club La Nación member cards and your ID card as proof of identity before asking for the check.', img:'img/julio.gif' },
    { id: 1, name: 'Parollacia', description:'30 % of benefit by Rollasolution.', img:'img/parolacia.png' },
    { id: 2, name: 'Mc', description:'15 % off', img:'img/mc.jpg' },
    { id: 3, name: 'El Sanjuanino ', description:'Valid benefit by submitting Club La Nación PREMIUM or Club La Nación member cards and your ID card as proof of identity before asking for the check.', img:'img/sanjuanino.jpg' },
    { id: 4, name: 'El almacen', description:'30 % of benefit by Rollasolution.', img:'img/ElViejoAlmacen.jpg' },  
  ];

  return {
    all: function() {
      return items;
    },
    get: function(id) {
      // Simple index lookup
      return items[id];
    }
  }
});

